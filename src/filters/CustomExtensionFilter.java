package filters;

import java.io.File;
import java.io.FileFilter;

public class CustomExtensionFilter implements FileFilter {
    
    private final String[] acceptedExtensions;
    
    public CustomExtensionFilter(String[] acceptedExtensions) {
        this.acceptedExtensions = acceptedExtensions;
    }

    @Override
    public boolean accept(File pathname) {
        return(fileEndsWithAcceptedExtension(pathname.getName()));
    }
    
    private boolean fileEndsWithAcceptedExtension(String fileName) {
        String fileExtension = getExtensionFor(fileName);
        for (String acceptedExtension : acceptedExtensions) {
            if (acceptedExtension.equals(fileExtension)) {
                return true;
            }
        }
        return false;
    }
    
    private String getExtensionFor(String fileName) {
        return "";
    }
    
}
