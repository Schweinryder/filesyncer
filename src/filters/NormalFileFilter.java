package filters;

import java.io.File;
import java.io.FileFilter;

public class NormalFileFilter implements FileFilter {

    @Override
    public boolean accept(File pathname) {
        if (pathname.isDirectory()) {
            return false;
        }        
        return pathname.exists();
    }
    
}
