package filesyncer;

import filters.NormalFileFilter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class FileSynchronizer extends Synchronizer {
    
    public FileSynchronizer() {
        this.filter = new NormalFileFilter();
    }

    @Override
    protected void startSynchronization() {
        //synchronizeTarget(sourceDirectory, targetDirectory);
        alternativeSynchronization(sourceDirectory, targetDirectory);
    }
    
    private void synchronizeTarget(File source, File target) {
        print("Source: " + source.getAbsolutePath());
        print("Target: " + target.getAbsolutePath());
        
        StringBuilder existing = new StringBuilder("Exists:\n");
        StringBuilder missing = new StringBuilder("Missing:\n");
        List<String> missingFiles = new ArrayList<>();
        
        File[] sourceFiles = source.listFiles(filter);
        for (File file : sourceFiles) {
            String fileName = file.getName();
            boolean exists = FileService.doesFileExistInDirectory(fileName, target);
            if (exists) {
                existing.append(fileName).append(", ");
            } else {
                missing.append(fileName).append(", ");
                missingFiles.add(fileName);
            }
        }
        System.out.println("Files found: " + sourceFiles.length);
        System.out.println(existing.toString());
        System.out.println(missing.toString());
        
        copyMissingFiles(source, target, missingFiles);
        copyMissingFilesInSubFolders(source, target);
    }
    
    private void copyMissingFiles(File source, File target, List<String> missingFiles) {
        missingFiles.stream().forEach((missingFile)  -> {
            copyMissingFile(source, target, missingFile);
        });
    }
    
    private void copyMissingFile(File source, File target, String missingFile) {
        File sourceFile = FileService.getFile(source, missingFile);
        File targetFile = FileService.getFile(target, missingFile);
        if (!sourceFile.exists() || targetFile.exists()) {
            print("Epic fail..");
            return;
        }
        try {
            Files.copy(sourceFile.toPath(), targetFile.toPath());
        } catch (IOException ioe) {
            print("SUuper epic failes: " + ioe.getMessage());
        }
    }
    
    private void copyMissingFilesInSubFolders(File source, File target) {
        File[] sourceSubdirectories = DirectoryService.listFiles(source);
        File[] targetSubdirectories = DirectoryService.listFiles(target);
        List<DirectorySet> sets = DirectoryService.createSubDirectorySets(sourceSubdirectories, targetSubdirectories);
        sets.stream().forEach((set) -> {
            synchronizeTarget(set.sourceDirectory(), set.targetDirectory());
        });
    }
    
    private void alternativeSynchronization(File sourceDirectory, File targetDirectory) {
        List<File> missingFiles = getFilesMissingInTargetDirectory(sourceDirectory, targetDirectory);
        print("Missing FILES:");
        missingFiles.stream().forEach((missingFile) -> {
            FileService.getFileChecksum(missingFile, true);
        });
    }
    
    private List<File> getFilesMissingInTargetDirectory(File sourceDirectory, File targetDirectory) {
       List<File> missingFiles = new ArrayList<>();
        
        File[] sourceFiles = sourceDirectory.listFiles(filter);
        for (File file : sourceFiles) {
            boolean exists = FileService.doesFileExistInDirectory(file.getName(), targetDirectory);
            if (!exists) {
                missingFiles.add(file);
            }
        }
        missingFiles.addAll(getMissingFilesInSubDirectories(sourceDirectory, targetDirectory));
        return missingFiles;
    }
    
    private List<File> getMissingFilesInSubDirectories(File source, File target) {
        List<File> missingFiles = new ArrayList<>();
        File[] sourceSubdirectories = DirectoryService.listFiles(source);
        File[] targetSubdirectories = DirectoryService.listFiles(target);
        List<DirectorySet> sets = DirectoryService.createSubDirectorySets(sourceSubdirectories, targetSubdirectories);
        sets.stream().forEach((set) -> {
            missingFiles.addAll(getFilesMissingInTargetDirectory(set.sourceDirectory(), set.targetDirectory()));
        });
        return missingFiles;
    }
    
}
