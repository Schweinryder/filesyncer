package filesyncer;

import java.io.File;

public class DirectorySet {
    
    private final File sourceDirectory;
    private final File targetDirectory;
    
    public DirectorySet(File sourceDirectory, File targetDirectory) {
        this.sourceDirectory = sourceDirectory;
        this.targetDirectory = targetDirectory;
    }
    
    public File sourceDirectory() {
        return sourceDirectory;
    }
    
    public File targetDirectory() {
        return targetDirectory;
    }
    
    @Override
    public String toString() {
        StringBuilder info = new StringBuilder("--Directory set--\n");
        info.append("Source: ").append(sourceDirectory.getAbsolutePath()).append("\n");
        info.append("Target: ").append(targetDirectory.getAbsolutePath()).append("\n");
        return info.toString();
    }
    
}
