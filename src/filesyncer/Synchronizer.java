package filesyncer;

import java.io.File;
import java.io.FileFilter;

public abstract class Synchronizer implements ConsolePrinter {
    
    FileFilter filter;
    File sourceDirectory;
    File targetDirectory;

    public void setSourceDirectory(File sourceDirectory) {
        this.sourceDirectory = sourceDirectory;
    }

    public void setTargetDirectory(File targetDirectory) {
        this.targetDirectory = targetDirectory;
    }
    
    protected abstract void startSynchronization();
    
}
