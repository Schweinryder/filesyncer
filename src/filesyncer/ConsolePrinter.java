package filesyncer;

public interface ConsolePrinter {
    
    public default void print(String text) {
        System.out.println(text);
    }
    
}
