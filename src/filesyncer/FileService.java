package filesyncer;

import filters.DirectoryFilter;
import filters.NormalFileFilter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.Arrays;

public class FileService implements ConsolePrinter {
    
    public static final String ROOT_PATH = "C:\\Users\\Webe\\Documents\\Test";    
    public static final String DIRECTORY_FOO = "FooFolder";
    public static final String DIRECTORY_BAR = "BarFolder";
    
    
    public static boolean doesDirectoryExistInDirectory(String directoryName, File directory) {
        if (directory == null || directoryName == null || directoryName.isEmpty()) {
            return false;
        }
        for (File dirInDir : directory.listFiles(new DirectoryFilter())) {
            if (dirInDir.getName().equals(directoryName)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean doesFileExistInDirectory(String fileName, File directory) {
        //System.out.println("Directory: " + directory.getAbsolutePath());
        for (File fileInDir : directory.listFiles(fileFilter())) {
           //System.out.println("File: " + fileInDir.getAbsolutePath());
            if (fileInDir.getName().equals(fileName)) {
                return true;
            }
            /*if (isIdentical(fileInDir, fileName)) {
                return true;
            }*/
        }
        return false;
    }
    
    private static FileFilter fileFilter() {
        return new NormalFileFilter();
    }
    
    private static FileFilter directoryFilter() {
        return new DirectoryFilter();
    }
    
    private static boolean isIdentical(File file1, File file2) {
        boolean isIdentical = true;
        isIdentical &= file1.getName().equals(file2.getName());
        isIdentical &= file1.length() == file2.length();
        return isIdentical;
    }
    
    public static File getFile(File directory, String fileName) {
        StringBuilder filePath = new StringBuilder();
        filePath.append(DirectoryService.getPathWithSeparator(directory));
        filePath.append(fileName);
        return new File(filePath.toString());
    }
    
    public static String getFileChecksum(File file) {
        return getFileChecksum(file, false);
    }
    
    public static String getFileChecksum(File file, boolean printHash) {
        String checksum = "";
        try {
            checksum = getHash(file);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        if (printHash) {
            System.out.println("Checksum: " + checksum + " (File \"" + file.getName() + "\")");
        }
        return checksum;
    }
    
    private static String getHash(File file) throws Exception {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        updateMessageDigest(messageDigest, file);
        return getHashString(messageDigest.digest());
    }
    
    private static void updateMessageDigest(MessageDigest messageDigest, File file) throws Exception {
        try (FileInputStream fis = new FileInputStream(file)) {
            byte[] byteArray = new byte[1024];
            int bytesCount = 0;
            while ((bytesCount = fis.read(byteArray)) != -1) {
                messageDigest.update(byteArray, 0, bytesCount);
            }
        }
    }
    
    private static String getHashString(byte[] digest) {
        StringBuilder hashBuilder = new StringBuilder();
        for(int i=0; i< digest.length ;i++) {
            hashBuilder.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
        }
        return hashBuilder.toString();
    }
    
}
