package filesyncer;

public enum SyncMode {
    COPY_EVERYTHING,
    SKIP_MOVED_FILES,
}
