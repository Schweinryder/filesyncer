package filesyncer;

import filters.DirectoryFilter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DirectorySynchronizer extends Synchronizer {
    
    public DirectorySynchronizer() {
        this.filter = new DirectoryFilter();
    }
    
    @Override
    public void startSynchronization() {
        synchronizeTarget(sourceDirectory, targetDirectory);
    }
    
    private void synchronizeTarget(File source, File target) {
        //print("Source: " + source.getAbsolutePath());
        //print("Target: " + target.getAbsolutePath());
        
        StringBuilder existing = new StringBuilder("Exists:\n");
        StringBuilder missing = new StringBuilder("Missing:\n");
        List<String> missingDirectories = new ArrayList<>();
        
        File[] sourceDirectories = source.listFiles(filter);
        for (File directory : sourceDirectories) {
            boolean exists = FileService.doesDirectoryExistInDirectory(directory.getName(), target);
            if (exists) {
                existing.append(directory.getName()).append(", ");
            } else {
                missing.append(directory.getName()).append(", ");
                missingDirectories.add(directory.getName());
            }
        }
        //System.out.println("Directories found: " + sourceDirectories.length);
        //System.out.println(existing.toString());
        //System.out.println(missing.toString());
        
        createMissingDirectoriesIn(target, missingDirectories);
        synchronizeTargetSubDirectories(createSubDirectorySets(sourceDirectories, target.listFiles(filter)));
    }
    
    private List<DirectorySet> createSubDirectorySets(File[] sourceSubdirectories, File[] targetSubDirectories) {
        List<DirectorySet> subDirectorySets = new ArrayList<> ();
        for (File sourceSubDirectory : sourceSubdirectories) {
            File targetSubDirectory = getDirectoryWithName(targetSubDirectories, sourceSubDirectory.getName());
            subDirectorySets.add(new DirectorySet(sourceSubDirectory, targetSubDirectory));
        }
        return subDirectorySets;
    }
    
    private File getDirectoryWithName(File[] directories, String name) {
        for (File directory : directories) {
            if (DirectoryService.isNameEqual(directory.getName(), name)) {
                return directory;
            }
        }
        return null;
    }
    
    private void synchronizeTargetSubDirectories(List<DirectorySet> subDirectorySets) {
        subDirectorySets.stream().forEach((set) -> {
            synchronizeTarget(set.sourceDirectory(), set.targetDirectory());
        });        
    }
    
    private void createMissingDirectoriesIn(File directory, List<String> missingSubdirectories) {
        missingSubdirectories.stream().forEach((missingSubdirectory) -> {
            createDirectory(getPathFor(directory) + missingSubdirectory);
        });
    }
    
    private void createDirectory(String path) {
        File newDirectory = new File(path);
        newDirectory.mkdir();
        print(newDirectory.getAbsolutePath() + " created!");
    }
    
    private String getPathFor(File directory) {
        String path = directory.getAbsolutePath();
        path = path.endsWith(File.separator) ? path : path.concat(File.separator);
        return path;
    }
    
}
