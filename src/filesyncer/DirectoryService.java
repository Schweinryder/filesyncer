package filesyncer;

import filters.DirectoryFilter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DirectoryService {
    
    public static boolean isNameEqual(String directoryName1, String directoryName2) {
        String baseDirectoryName = getDirectoryNameWithSeparator(directoryName1);
        String compareDirectoryName = getDirectoryNameWithSeparator(directoryName2);
        return baseDirectoryName.equals(compareDirectoryName);
    }
    
    public static String getDirectoryNameWithSeparator(String directoryName) {
        return directoryName.endsWith(File.separator) ? directoryName : directoryName + File.separator;
    }
    
    public static String getPathWithSeparator(File directory) {
        String path = directory.getAbsolutePath();
        path = path.endsWith(File.separator) ? path : path + File.separator;
        return path;
    }
    
    public static List<DirectorySet> createSubDirectorySets(File[] sourceSubdirectories, File[] targetSubDirectories) {
        List<DirectorySet> subDirectorySets = new ArrayList<> ();
        for (File sourceSubDirectory : sourceSubdirectories) {
            File targetSubDirectory = getDirectoryWithName(targetSubDirectories, sourceSubDirectory.getName());
            subDirectorySets.add(new DirectorySet(sourceSubDirectory, targetSubDirectory));
        }
        return subDirectorySets;
    }
    
    public static File getDirectoryWithName(File[] directories, String name) {
        for (File directory : directories) {
            if (isNameEqual(directory.getName(), name)) {
                return directory;
            }
        }
        return null;
    }
    
    public static File[] listFiles(File directory) {
        return directory.listFiles(new DirectoryFilter());
    }
    
}
