package filesyncer;

import java.io.File;

public class FileSet {
    
    private final File sourceFile;
    private final File targetFile;
    
    public FileSet(File sourceFile, File targetFile) {
        this.sourceFile = sourceFile;
        this.targetFile = targetFile;
    }
    
    public File sourceFile() {
        return sourceFile;
    }
    
    public File targetFile() {
        return targetFile;
    }
    
    @Override
    public String toString() {
        StringBuilder info = new StringBuilder("--File set--\n");
        info.append("Source: ").append(sourceFile.getAbsolutePath()).append("\n");
        info.append("Target: ").append(targetFile.getAbsolutePath()).append("\n");
        return info.toString();
    }
    
}
