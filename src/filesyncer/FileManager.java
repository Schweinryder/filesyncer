package filesyncer;

import java.io.File;

public class FileManager implements ConsolePrinter {
    
    public void runTest() {
        String fooPath = FileService.ROOT_PATH + File.separator + FileService.DIRECTORY_FOO;
        String barPath = FileService.ROOT_PATH + File.separator + FileService.DIRECTORY_BAR;
        File fooDirectory = new File(fooPath);
        File barDirectory = new File(barPath);
        
        //synchronize(fooDirectory, barDirectory);
        synchronize(barDirectory, fooDirectory);
    }
    
    private void synchronize(File sourceDirectory, File targetDirectory) {
        synchronizeDirectories(sourceDirectory, targetDirectory);
        synchronizeFiles(sourceDirectory, targetDirectory);
    }
    
    private void synchronizeDirectories(File sourceDirectory, File targetDirectory) {
        DirectorySynchronizer directorySyncer = new DirectorySynchronizer();
        directorySyncer.setSourceDirectory(sourceDirectory);
        directorySyncer.setTargetDirectory(targetDirectory);
        directorySyncer.startSynchronization();
    }
    
    private void synchronizeFiles(File sourceDirectory, File targetDirectory) {
        FileSynchronizer fileSyncronizer = new FileSynchronizer();
        fileSyncronizer.setSourceDirectory(sourceDirectory);
        fileSyncronizer.setTargetDirectory(targetDirectory);
        fileSyncronizer.startSynchronization();
    }
    
}
